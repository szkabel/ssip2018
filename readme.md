Readme on the CODE of the project in SSIP2018.

% ******** DATA ******** %
Our data was provided in .mat files.
Each .mat file contained 3 variables: I, Imask and examTable.
I is the 3D image, a 3D array that contain the MRI images. The z-stack is the 3rd dimension.
Imask contain the ROI, the region of interest again in a 3D array, however here the 3rd dimension is only as thick as many z-stack falls into the ROI. The ROI in our case was a 3D brick, rectangular in all projections.
The positioning of the ROI is given in the 3rd variable: examTable.
ExamTable is a structure with many fields. aclROIsliceFromInd to aclROIsliceToInd are the boundaries in the original I image for the ROI.
You can see 4 examples in the sampleData folder.

% ******** VISUALIZATION ******** %
A) API
We define the ROI-s in a ROI structure usually denoted with ROIstr.
The ROI structure has 6 fields: X,Y,Z for the top left most front corner identification and the Xw,Yw,Zw for the sizes of the project.

Then to visualize easily 3D images, we wrote the makeTileImage function. It takes the 3D array I and outputs an image where the slacks are tiled to a grid with as close to a square as possible.

B) GUI
Remember first to add the proper directories to the Matlab path by running startup in the main folder.
Given a folder with .mat files like in sampleData folder one can use our KneeSegmenter GUI to visualize the results.
Upon typing in KneeSegmenter it asks for such a folder.
You can find CODE for this is in the visualization folder.

You can load into the visualizer segmentations via this format. Segmentations result must be placed in a folder in separate .mat files for each image. The file should contain one single variable ROIstr, which is as described above. The file names must correspond and be exactly the same as in the original folder.
Then you can load multiple of such folders to compare your results.

Sample folders as such are in Segmentations.

% ******** METHODS ******** %
Our methods are rather simple and are placed in the SegmentationMethods folder.
We transformed the dataset so that we could feed it in into deep learning, and used the matlab demo for ROI detection.
See the MATLAB website:
https://www.mathworks.com/help/vision/ref/rcnnobjectdetector-class.html

We also implemented a dummy methods for recognizing, called segmentBySum and segmentBySumGradient.
Both take an image and outupts the ROI structure described above.

There are functions also that can run the above code on full folders see:
runSumSegmentOnFolder.m
runRCNNonFolder.m
