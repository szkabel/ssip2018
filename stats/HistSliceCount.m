function HistSliceCount
%This function asks from the user for an input folder and
%then converts all the mat files 

Ip = uigetdir('Please specify an input folder');
% Op = uigetdir('Please specify an output folder');

d = dir(fullfile(Ip,'*.mat'));

%initializing nofStacks
nofStacks=zeros(1,numel(d));

% getting a vector of noSlices of all the files in the folder
 for i=1:numel(d)
     load(fullfile(Ip,d(i).name))
     nofStacks(i) = examData.noSlices;
 end

%  drawing a histogram of noSlices
 breaks = (min(nofStacks)-0.5):(max(nofStacks)+0.5);
 histogram(nofStacks, breaks);

end