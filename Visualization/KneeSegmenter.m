function varargout = KneeSegmenter(varargin)
% KNEESEGMENTER MATLAB code for KneeSegmenter.fig
%      KNEESEGMENTER, by itself, creates a new KNEESEGMENTER or raises the existing
%      singleton*.
%
%      H = KNEESEGMENTER returns the handle to a new KNEESEGMENTER or the handle to
%      the existing singleton*.
%
%      KNEESEGMENTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in KNEESEGMENTER.M with the given input arguments.
%
%      KNEESEGMENTER('Property','Value',...) creates a new KNEESEGMENTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before KneeSegmenter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to KneeSegmenter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% uData = get(handles.figure1,'UserData');
% set(handles.figure1,'UserData',uData);


% Edit the above text to modify the response to help KneeSegmenter

% Last Modified by GUIDE v2.5 19-Jul-2018 01:05:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @KneeSegmenter_OpeningFcn, ...
                   'gui_OutputFcn',  @KneeSegmenter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before KneeSegmenter is made visible.
function KneeSegmenter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to KneeSegmenter (see VARARGIN)

% Choose default command line output for KneeSegmenter
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

dataDir = uigetdir(pwd,'Please specify the folder of the images');
%dataDir = '/home/szkabel/Documents/SSIP2018/data/series/PD_TSE_small_ones';

d = dir([dataDir filesep '*.mat']);

set(handles.listbox1,'String',{d.name});
uData.dataDir = dataDir;
uData.predictions = {}; %an empty cell array to initialize
uData.imgFormat = 'tiles';
set(handles.figure1,'UserData',uData);

imshow(ones(100,100,3).*0.2,'Parent',handles.axes1);
set(handles.radioTiles,'Enable','off');
set(handles.radioStack,'Enable','off');


% UIWAIT makes KneeSegmenter wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = KneeSegmenter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1

contents = cellstr(get(hObject,'String'));
selectedData = contents{get(hObject,'Value')};
set(handles.radioTiles,'Enable','on');
set(handles.radioStack,'Enable','on');

uData = get(handles.figure1,'UserData');
uData.selectedData = selectedData;
set(handles.figure1,'UserData',uData);
drawImage(handles);

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)if get(handles.groundTruthCheckBox,'Value')  
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in groundTruthCheckBox.
function groundTruthCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to groundTruthCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of groundTruthCheckBox
drawImage(handles);


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
placeItemsOnGUI(handles);


% --- Executes on button press in radioTiles.
function radioTiles_Callback(hObject, eventdata, handles)
% hObject    handle to radioTiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radioTiles

if get(hObject,'Value')
    uData = get(handles.figure1,'UserData');
    uData.imgFormat = 'tiles';
    set(handles.figure1,'UserData',uData);
    drawImage(handles);
end


% --- Executes on button press in radioStack.
function radioStack_Callback(hObject, eventdata, handles)
% hObject    handle to radioStack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radioStack

if get(hObject,'Value')
    uData = get(handles.figure1,'UserData');
    uData.imgFormat = 'stack';    
    set(handles.figure1,'UserData',uData);
    drawImage(handles);
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

uData = get(handles.figure1,'UserData');
uData.stack = round(get(hObject,'Value'));
set(handles.figure1,'UserData',uData);
drawImage(handles);



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on scroll wheel click while the figure is in focus.
function figure1_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

set(0,'Unit','Pixels');
p = get(0,'PointerLocation');
GUIP = get(handles.figure1,'Position');
GUIpos = get(handles.axes1,'Position');
if get(handles.radioStack,'Value') && p(1) >= GUIP(1)+GUIpos(1) && p(1)<=  GUIP(1) + GUIpos(1) +GUIpos(3) && ...
        p(2) >= GUIP(2)+GUIpos(2) && p(2)<=  GUIP(2) + GUIpos(2) +GUIpos(4)
    uData = get(handles.figure1,'UserData');  
    if uData.stack + eventdata.VerticalScrollCount > 0 && uData.stack + eventdata.VerticalScrollCount <= get(handles.slider1,'Max');
        uData.stack = uData.stack + eventdata.VerticalScrollCount;
    end
    set(handles.figure1,'UserData',uData);
    drawImage(handles);
end


% --- Executes on selection change in predictionListBox.
function predictionListBox_Callback(hObject, eventdata, handles)
% hObject    handle to predictionListBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns predictionListBox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from predictionListBox

drawImage(handles)


% --- Executes during object creation, after setting all properties.
function predictionListBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to predictionListBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{});


% --- Executes on button press in buttonAdd.
function buttonAdd_Callback(hObject, eventdata, handles)
% hObject    handle to buttonAdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');

d = uigetdir(pwd,'Specify the folder of predictions');
if d == 0, return; end
uData.predictions{end+1} = struct('dir',d);
cols = [0 1 0;...
          0 1 1;...
          0 0 1;...
          1 0 1];

uData.predictions{end}.color = cols(mod(length(uData.predictions),4)+1,:);

dirs = strsplit(d,filesep);
soFarList = get(handles.predictionListBox,'String');
soFarList{end+1} = dirs{end};
set(handles.predictionListBox,'String',soFarList);
set(handles.predictionListBox,'Value',length(soFarList));
set(handles.predictionListBox,'Max',length(soFarList));
set(handles.figure1,'UserData',uData);


% --- Executes on button press in buttonRemove.
function buttonRemove_Callback(hObject, eventdata, handles)
% hObject    handle to buttonRemove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
v = get(handles.predictionListBox,'Value');
if numel(v) == 1
   uData.predictions(v) = [];
   str = get(handles.predictionListBox,'String');
   str(v) = [];
   set(handles.predictionListBox,'String',str);
   set(handles.predictionListBox,'Value',length(str));
end
set(handles.figure1,'UserData',uData);
drawImage(handles);

% --- Executes on button press in buttonNone.
function buttonNone_Callback(hObject, eventdata, handles)
% hObject    handle to buttonNone (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.predictionListBox,'Value',[]);
drawImage(handles);
