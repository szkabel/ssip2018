function [a, b] = closestDiv(c)
% AUTHOR:   Abel Szkalisity
% DATE:     March 08, 2017
% NAME:     closestDivisor
%
% It finds the two closest divisor-pair of c.
% For example: if c is a squere then a=b=sqrt(c) or if c is a prime then
% a=1 b=c.
%
%
% INPUT:
%   c            	Number to which find the closest divisor pair
%
% OUTPUT:
%   a               The smaller divisor from the pair
%   b               The greater divisor from the pair
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

for i=1:c
    if mod(c,i)==0
        if i<sqrt(c)
            a=i;
        else
            b=i;
            if i==sqrt(c)
                a=i;
            end
            break;
        end
    end
end

end