function tileImg = makeTileImage(imgStck, ROIstrs, lineWidth)
%Creates a tileImg from the stack so it is easy to visualize in 2D
%All the time X and Y in matrix notation
%The 2nd parameter is a cellarray of the ROI structures
%If the last one is negative then we show the image
%If it is positive then we just give it back

Zstack = size(imgStck,3);
wX = size(imgStck,1);
wY = size(imgStck,2);

[a,b] = closestDiv(Zstack);

tileImg = zeros(wX*a,wY*b);

for i=1:a
    for j = 1:b
        tileImg((i-1)*wX+1:i*wX,(j-1)*wY+1:j*wY) = imgStck(:,:,(i-1)*b+j);
    end
end

if nargin >1  % if we have the ROIs also
    if lineWidth > 0
        adjustedImg = tileImg./(2^12-1);
        colorImg = repmat(adjustedImg,1,1,3);
        for i=1:a
            for j = 1:b
                for k=1:length(ROIstrs)
                    ROIstr = ROIstrs{k};
                    if (i-1)*b+j >= ROIstr.Z && (i-1)*b+j <= ROIstr.Z+ROIstr.Zw
                        Xspan = max(1,round((i-1)*wX+ROIstr.X-lineWidth/2) ) : min( i*wX, round((i-1)*wX+ROIstr.X+ROIstr.Xw + lineWidth/2));
                        Yspan = max(1,round((j-1)*wY+ROIstr.Y-lineWidth/2) ) : min( j*wY, round((j-1)*wY+ROIstr.Y+ROIstr.Yw + lineWidth/2));
                        colorImg(Xspan,[Yspan(1:round(lineWidth)) Yspan(end-round(lineWidth):end)],1) = ROIstr.color(1);
                        colorImg([Xspan(1:round(lineWidth)) Xspan(end-round(lineWidth):end)],Yspan,1) = ROIstr.color(1);
                        colorImg(Xspan,[Yspan(1:round(lineWidth)) Yspan(end-round(lineWidth):end)],2) = ROIstr.color(2);
                        colorImg([Xspan(1:round(lineWidth)) Xspan(end-round(lineWidth):end)],Yspan,2) = ROIstr.color(2);
                        colorImg(Xspan,[Yspan(1:round(lineWidth)) Yspan(end-round(lineWidth):end)],3) = ROIstr.color(3);
                        colorImg([Xspan(1:round(lineWidth)) Xspan(end-round(lineWidth):end)],Yspan,3) = ROIstr.color(3);
                    end
                end
            end
        end
        tileImg = colorImg;
    
    else
        
        imagesc(tileImg);
        for i=1:a
            for j = 1:b
                if (i-1)*b+j >= ROIstr.Z && (i-1)*b+j <= ROIstr.Z+ROIstr.Zw
                    Xspan = (i-1)*wX+ROIstr.X;
                    Yspan = (j-1)*wY+ROIstr.Y;
                    rectangle('Position',[Yspan,Xspan,ROIstr.Yw,ROIstr.Xw],'EdgeColor','r','LineWidth',-lineWidth);
                end
            end
        end
    end
end



end