matrix = xlsread('metadata_dane.xlsx');

row=883;

xROI=matrix(row,5);
yROI=matrix(row,6);
zROI=matrix(row,7);

ROIwidth=matrix(row,9); % connected with "X" coordinate
ROIheight=matrix(row,8); % connected with "Y" coordinate

ROIslices=matrix(row,10);

% "X" coordinate
% ---------------

xmin=-xROI-ROIwidth; 
xmax=-xROI;

% values in 3D Slicer files 

x1=0.5*(xmin+xmax)
x2=0.5*(xmax-xmin)


% "Y" coordinate
% ---------------

ymin=-yROI-ROIheight; 
ymax=-yROI;

% values in 3D Slicer files 
y1=0.5*(ymin+ymax)
y2=0.5*(ymax-ymin)





% "Z" coordinate
% ----------------------------
% values in generated notebooks

zmin=zROI; 
zmax=zROI+ROIslices;

% values in 3D Slicer files 
z1=0.5*(zmin+zmax)
z2=0.5*(zmax-zmin) 

zslices=zmax-zmin % amount of "Z" slices 



