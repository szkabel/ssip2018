Short description to the CODE knee segmenter.
It is a GUI created in Matlab GUI designer GUIDE.
It waits for a folder that contains .mat files that needs to be read in and which forms the full data set.
The .mat files contain I(mage) data and an examTable variable that describe the annotated data of ligaments.

You can load in predictions for this image set by adding a folder that contains again .mat files for the predicitons.
The .mat files in the predictions describe the data in the ROI structure format defined in the convertSeriesToROI code.
The .mat files must have the exact same name as the original images (.mat files). However we don't need a .mat file for each image, it is enough to have it for a subset. Both the missing file and the empty ROIstr can indicate empty finding.
The .mat file must have a variable called ROIstr which describes the identified ROI.
