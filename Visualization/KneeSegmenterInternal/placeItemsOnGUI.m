function handles = placeItemsOnGUI(handles)

GUIpos = get(handles.figure1,'Position');

padding = 10;
rightMenuSize = 150;
topMenuSize = 100;

set(handles.axes1,'Position',[padding padding GUIpos(3)-padding*3-rightMenuSize GUIpos(4)-padding*3-topMenuSize]);
set(handles.listbox1,'Position',[GUIpos(3)-padding-rightMenuSize padding rightMenuSize GUIpos(4)-padding*3-topMenuSize]);

%Top menu elements
nextElement = 150;
set(handles.uibuttongroup1,'Position',[padding GUIpos(4)-padding-topMenuSize nextElement topMenuSize]);
set(handles.groundTruthCheckBox,'Position',[padding + nextElement + padding GUIpos(4)-padding-topMenuSize nextElement topMenuSize]);
set(handles.predictionListBox,'Position',[padding*3 + nextElement*2 GUIpos(4)-padding-topMenuSize nextElement topMenuSize ]);
set(handles.buttonAdd,'Position',[padding*4 + nextElement*3 GUIpos(4)-padding-topMenuSize/3 nextElement topMenuSize/3 ]);
set(handles.buttonRemove,'Position',[padding*4 + nextElement*3 GUIpos(4)-padding-topMenuSize nextElement topMenuSize/3 ]);
set(handles.buttonNone,'Position',[padding*4 + nextElement*3 GUIpos(4)-padding-topMenuSize/3*2 nextElement topMenuSize/3 ]);

%If slidebar is visible
if strcmp(get(handles.slider1,'Visible'),'on')
    slideBarHeight = 20;
    set(handles.axes1,'Position',[padding padding+slideBarHeight GUIpos(3)-padding*3-rightMenuSize GUIpos(4)-padding*3-topMenuSize-slideBarHeight]);
    set(handles.slider1,'Position',[padding padding GUIpos(3)-padding*3-rightMenuSize slideBarHeight]);
end


end
