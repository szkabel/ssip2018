function ROIstrs = fetchPredROIstrs(predictions, predArray, imageID)
    ROIstrs = cell(length(predictions),1);
    j = 1;
    for i=1:length(predictions)
        if exist(fullfile(predArray{predictions(i)}.dir,imageID),'file')
        load(fullfile(predArray{predictions(i)}.dir,imageID));
            if ~isempty(ROIstr) %only if we have found sg there
                ROIstrs{j} = ROIstr;
                ROIstrs{j}.color = predArray{predictions(i)}.color;
                j = j+1;
            else
                ROIstrs(j) = [];    
            end
        else
            ROIstrs(j) = [];
        end
    end
end