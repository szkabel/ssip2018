function drawImage( handles )
%draws the image in the appropriate format to the axes visible in the image
%all data is extracted from the handles

uData = get(handles.figure1,'UserData');

load(fullfile(uData.dataDir,uData.selectedData));
    
predictions = get(handles.predictionListBox,'Value');

%if we see in tiles
if strcmp(uData.imgFormat,'tiles')
   set(handles.slider1,'Visible','off');
   if get(handles.groundTruthCheckBox,'Value')
        ROIstr = convertSeriesToROI(examData);
        ROIstr.color = [1 0 0];        
        if isempty(get(handles.predictionListBox,'String'))
            tileImg = makeTileImage(I, {ROIstr}, 6);
        else
            predROIstrs = fetchPredROIstrs(predictions,uData.predictions,uData.selectedData);        
            tileImg = makeTileImage(I, {ROIstr predROIstrs{:}}, 6);
        end        
        imshow(tileImg,'Parent',handles.axes1);
   elseif ~isempty(predictions) && ~isempty(get(handles.predictionListBox,'String'))
        predROIstrs = fetchPredROIstrs(predictions,uData.predictions,uData.selectedData);        
        tileImg = makeTileImage(I, predROIstrs, 6);
        imshow(tileImg,'Parent',handles.axes1);
   else
        tileImg = makeTileImage(I);
        imshow((tileImg./(2^12-1)),'Parent',handles.axes1);
   end
elseif strcmp(uData.imgFormat,'stack')
    set(handles.slider1,'Visible','on');
    set(handles.slider1,'Min',1);
    set(handles.slider1,'Max',size(I,3));
    set(handles.slider1,'SliderStep',[1/(size(I,3)-1) 1/(size(I,3)-1)]);
    if ~isfield(uData,'stack')
        uData.stack = 1;
    elseif uData.stack > size(I,3)
        uData.stack = size(I,3);
    end
    set(handles.slider1,'Value',uData.stack);
    imshow(double(I(:,:,uData.stack))./(2^12-1),'Parent',handles.axes1);
    if get(handles.groundTruthCheckBox,'Value')        
        ROIstr = convertSeriesToROI(examData);
        if uData.stack >= ROIstr.Z && uData.stack <= ROIstr.Z+ROIstr.Zw;
            rectangle('Parent',handles.axes1,'Position',[ROIstr.Y,ROIstr.X,ROIstr.Yw,ROIstr.Xw],'EdgeColor',[1 0 0],'LineWidth',3);
        end
    end
end

set(handles.figure1,'UserData',uData);


placeItemsOnGUI(handles);


end

