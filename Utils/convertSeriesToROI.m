function str = convertSeriesToROI(examData)
%str is a structure that describes the 3D ROI by the following means
% .X .Y .Z are in matrix notation
% .Xw .Yw and .Zw are the size in each direction
% The size is such that X:X+Xw covers the whole region


[str.X, str.Y] = ind2sub(size(examData.aclROImask{1}),find(examData.aclROImask{1},1));
str.Z = examData.aclROIsliceFromInd;

str.Xw = sum(examData.aclROImask{1}(:,str.Y)) - 1;
str.Yw = sum(examData.aclROImask{1}(str.X,:)) - 1; %so that the width means similar thing as in the Z
str.Zw = examData.aclROIsliceToInd - examData.aclROIsliceFromInd;
str.color = [1 0 0];


end