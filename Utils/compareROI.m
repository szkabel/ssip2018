function [e1, e2,e3, missed]=compareROI
% takes three fo=lders as an input, uses __examtbl.mat to
% extract information about the maximal and minimal x and y
% values. uses these to calculate the error as:
% 

I1 = uigetdir(pwd,'Please specify input folder of original ROIs:');
I2 = uigetdir(pwd,'Please specify input folder of examtbl:');
I3 = uigetdir(pwd,'Please specify input folder of predicted ROIs:');
d1 = dir(fullfile(I1,'*.mat'));
d2 = dir(fullfile(I2,'__examtbl.mat'));
d3 = dir(fullfile(I3,'*.mat'));

load(fullfile(I2,d2.name));

% preallocate the variables

roiDepth=zeros(1,numel(d1));
roiArea=zeros(1,numel(d1));
Xmin1=(zeros(1,numel(d1)));
Xmax1=(zeros(1,numel(d1)));
Ymin1=(zeros(1,numel(d1)));
Ymax1=(zeros(1,numel(d1)));
Zmin1=(zeros(1,numel(d1)));
Zmax1=(zeros(1,numel(d1)));

Xmin2=(zeros(1,numel(d3)));
Xmax2=(zeros(1,numel(d3)));
Ymin2=(zeros(1,numel(d3)));
Ymax2=(zeros(1,numel(d3)));
Zmin2=(zeros(1,numel(d3)));
Zmax2=(zeros(1,numel(d3)));

missed=0;

n=size(examtbl.examID,1);

% go through the examtable and locate a matching examID

for i=1:numel(d1)
    load(fullfile(I1,d1(i).name));
   
    for j=1:n
        if examData.examID==examtbl.examID(j)
            break
        end
    end
    
% calculate ROI volume, and  the maximal and minimal x and y
% values of the ROI
    
     roiDepth(i)=double(examtbl.aclROIsliceToInd(j)-examtbl.aclROIsliceFromInd(j));
     roiArea(i)=double(examtbl.aclROIarea(j));

     Xmin1(i)=double(examtbl.aclROIslicePosition(j,1));
     Xmax1(i)=double(Xmin1(i)+examtbl.aclROIsliceWidth(j));
      
     Ymin1(i)=double(examtbl.aclROIslicePosition(j,2));
     Ymax1(i)=double(Ymin1(i)+examtbl.aclROIsliceHeight(j));
     
     Zmin1(i)=double(examtbl.aclROIsliceFromInd(j));
     Zmin2(i)=double(examtbl.aclROIsliceToInd(j));
end

%load the mat files containing predicted data
% and form vectors Xmin2, Xmax2, Ymin2, Ymax2

for k=1:numel(d3)
    load(fullfile(I3,d3(k).name));
    
    if isempty(ROIstr)
        continue
    end
    
    Xmin2(k)= ROIstr.X;
    Xmax2(k)=Xmin2(k)+ROIstr.Xw;
    
    Xmin2(k)=ROIstr.X;
    Xmax2(k)=Xmin2(k)+ROIstr.Xw;
    
    Ymin2(k)=ROIstr.Y;
    Ymax2(k)=Ymin2(k)+ROIstr.Yw;
    
    Zmin2(k)=ROIstr.Z;
    Zmax2(k)=Zmin2(k)+ROIstr.Zw;
end

missedSlices=0; errors=[];
for k=1:numel(d3)
    if Xmin2(k)==0
        missedSlices=missedSlices+1;
    end
    deltaX1(k)=abs(Xmin2(k)-Xmin1(k));
    deltaX2(k)=abs(Xmax2(k)-Xmax1(k));
    deltaY1(k)=abs(Ymin2(k)-Ymin1(k));
    deltaY2(k)=abs(Ymax2(k)-Ymax1(k));
    deltaZ1(k)=abs(Zmin2(k)-Zmin1(k));
    deltaZ2(k)=abs(Zmax2(k)-Zmax1(k));
end

deltaX=max(deltaX1,deltaX2);
deltaY=max(deltaY1,deltaY2);
deltaZ=max(deltaZ1,deltaZ2);

disp(Xmin2);
disp(Xmax2);

 e1=sqrt(deltaX.^2+deltaY.^2+deltaZ.^2);
 disp(max(e1));
e2=deltaX.*deltaY.*deltaZ;
disp(max(e2));
e3=max(deltaX, max(deltaY,deltaZ));
disp(max(e3));

missed=missedSlices;