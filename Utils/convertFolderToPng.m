function convertFolderToPng
%This function asks from the user for an input folder and a destination and
%then converts all the mat files from the input folder to the destination
%one

Ip = uigetdir(pwd,'Please specify an input folder');
Op = uigetdir(Ip,'Please specify an output folder');

d = dir(fullfile(Ip,'*.mat'));

for i=1:numel(d)
    load(fullfile(Ip,d(i).name));
    nofStacks = examData.noSlices;
    [~,fileEx,~] = fileparts(d(i).name);
    for j=1:nofStacks
        imwrite(I(:,:,j),fullfile(Op,[fileEx '-' num2str(j,'%03d') '.png']));
    end
end



end