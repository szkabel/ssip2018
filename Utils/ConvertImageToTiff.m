function ConvertImageToTiff(I,fileName,nofStacks)
%This function converts the input image from the mat file to a multi-stack
%tiff that can be visualized in Fiji e.g.

imwrite(I(:,:,1),fileName);
for i=2:nofStacks
    imwrite(I(:,:,i),fileName,'WriteMode','append');
end

end
