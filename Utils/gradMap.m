function gradmag = gradMap(A)

% B=grs2rgb(imadjust(A), colormap(hot(1000)));
hy = fspecial('sobel');
hx = hy';
Iy = imfilter(double(imadjust(A)), hy, 'replicate');
Ix = imfilter(double(imadjust(A)), hx, 'replicate');
gradmag = sqrt(Ix.^2 + Iy.^2);