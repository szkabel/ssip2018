function classifySamples

[f,p] = uigetfile('*.mat','Please specify the example table mat file');

source = uigetdir(p,'Please specify the source for the data');
dest = uigetdir(p,'Please specify a destination for the data');

load(fullfile(p,f));

h = waitbar(0,'Copying images');
for i=1:numel(examtbl.seriesNo)
    if exist([source filesep num2str(examtbl.examID(i)) '-' num2str(examtbl.seriesNo(i)) '.mat'],'file')
        movefile([source filesep num2str(examtbl.examID(i)) '-' num2str(examtbl.seriesNo(i)) '.mat'],[dest filesep num2str(examtbl.examID(i)) '-' num2str(examtbl.seriesNo(i)) '.mat']);
    else
        disp(['Missing file ' source filesep num2str(examtbl.examID(i)) '-' num2str(examtbl.seriesNo(i)) '.mat']);
    end
    waitbar(i/numel(examtbl.seriesNo),h,sprintf('Moving images %d/%d',i,numel(examtbl.seriesNo)));
end
if ishandle(h), close(h); end





end