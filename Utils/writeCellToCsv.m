function writeCellToCsv(cellarray, filename)
    f = fopen(filename,'w');
    for i=1:numel(cellarray)
        fprintf(f,'%s\n',cellarray{i});
    end
    fclose(f);
end