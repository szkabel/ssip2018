function ROIstr = segmentBySums(I)
%I is the 3D image, and we try to segment the crucial ligament by simple
%sum plots of the image

SUMpl = [];
for i=1:size(I,3)
    SUMpl(i,:) = sum(I(:,:,i),2);    
end

filSUMpl = imgaussfilt(SUMpl,7);
for i=1:size(filSUMpl,2)
    filSUMpl(:,i) = filSUMpl(:,i) + i;
end

[~,maxidx] = max(filSUMpl(:));
[ZstackCenter,~] = ind2sub([size(I,3),size(I,2)],maxidx);

[Xmin,Xmax] = findLocMins(filSUMpl(ZstackCenter,:));

ROIstr.X = Xmin;
ROIstr.Xw = Xmax-Xmin;
ROIstr.Y = ROIstr.X;
ROIstr.Yw = ROIstr.Xw;
ROIstr.Z = ZstackCenter - 2;
ROIstr.Zw = 4;
ROIstr.color = [0 1 0];

end