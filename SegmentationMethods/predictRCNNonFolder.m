function predictRCNNonFolder(rcnn,imageFolder,imageNames, outputFolder)
%We need the trained network RCNN, the folder of the images, the name of
%the images for which the prediction must be done and the output folder
%where we should place them.
%We assume that the data is in .mat format.

h = waitbar(0);
for i=1:numel(imageNames)
    load(fullfile(imageFolder,imageNames{i}));
    ROIstr = predictRCNN(rcnn,I);    
    save(fullfile(outputFolder,imageNames{i}),'ROIstr');    
    waitbar(i/numel(imageNames),h,sprintf('Predict RCNN %d/%d',i,numel(imageNames)));    
end

close(h);
end