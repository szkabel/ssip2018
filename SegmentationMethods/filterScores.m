function foundSg = filterScores(allScores,thresh)
%this function takes all of the scores and then gives back a binary array
%where the stacks that are considered to be ROI are consecutively indexed
%by one.

aboveThresh = allScores>thresh;

aboveThreshFilled = medfilt1(double(aboveThresh)); %fill in single holes

labelled = bwlabel(aboveThreshFilled);
R = regionprops(labelled);

[~,maxindex] = max(cell2mat({R.Area}));
if ~isempty(maxindex)
    foundSg = (labelled == maxindex);
    foundSg(aboveThresh == 0) = 0; %cancel out regions
else
    foundSg = 0;
end


end