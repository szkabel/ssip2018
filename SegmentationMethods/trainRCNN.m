function rcnn = trainRCNN(trainTable)
%trains the RCNN network

%options are taken from example
options = trainingOptions('sgdm', ...
    'MiniBatchSize', 32, ...
    'InitialLearnRate', 1e-6, ...
    'MaxEpochs', 100, ...
    'Verbose', true);

%load in also the layers
load('rcnnStopSigns.mat','layers')

layers(15) = classificationLayer();

rcnn = trainRCNNObjectDetector(trainTable, layers, options, 'NegativeOverlapRange', [0 0.3]);

end

    