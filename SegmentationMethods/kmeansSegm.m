%script to test kmeans
% I is the image

figure; imagesc(makeTileImage(I));

%{
%first with 27 neighbourhood (8 in 3D)
disp('Extract info from I(mage)');
N = numel(I);
padded = zeros(size(I)+2);
padded(2:end-1,2:end-1,2:end-1) = I;
X = zeros(N,27);
for i=1:N
    [x,y,z] = ind2sub(size(I),i);
    xP = x+1; yP = y+1; zP = z+1;
    X(i,:) = reshape(padded(xP-1:xP+1,yP-1:yP+1,zP-1:zP+1),1,27);
end
% }

disp('Kmeans');
k = 5;
ind = kmeans(X,k);

labelImg = zeros(size(I));

for i=1:k
    labelImg(ind == i) = i;
end

figure; imagesc(makeTileImage(labelImg));
%}

%{
%using spacial info
N = numel(I);
X = zeros(N,4);
disp('Copy info to array');
for i=1:N
    [x,y,z] = ind2sub(size(I),i);    
    X(i,:) = [I(x,y,z) x y z];
end

disp('Kmeans');
k = 5;
ind = kmeans(X,k);

labelImg = zeros(size(I));

for i=1:k
    labelImg(ind == i) = i;
end

figure; imagesc(makeTileImage(labelImg));
%}

% {
%6 neighbourhood (4 in 3D) AAAND additionally some spatial info
disp('Extract info from I(mage)');
N = numel(I);
padded = zeros(size(I)+2);
padded(2:end-1,2:end-1,2:end-1) = I;
X = zeros(N,10);
for i=1:N
    [x,y,z] = ind2sub(size(I),i);
    xP = x+1; yP = y+1; zP = z+1;
    X(i,:) = [I(x,y,z),...
        padded(xP-1,yP,zP),...
        padded(xP+1,yP,zP),...
        padded(xP,yP-1,zP),...
        padded(xP,yP+1,zP),...
        padded(xP,yP,zP-1),...
        padded(xP,yP,zP+1),...
        x y z];
end
% }

disp('Kmeans');
k = 5;
ind = kmeans(X,k);

labelImg = zeros(size(I));

for i=1:k
    labelImg(ind == i) = i;
end

figure; imagesc(makeTileImage(labelImg));
%}

%{
%6 neighbourhood (4 in 3D) AAAND additionally some spatial info
disp('Extract info from I(mage)');
N = numel(I);
cP = size(I)./2;
padded = zeros(size(I)+2);
padded(2:end-1,2:end-1,2:end-1) = I;
X = zeros(N,2);
for i=1:N
    [x,y,z] = ind2sub(size(I),i);
    xP = x+1; yP = y+1; zP = z+1;
    X(i,:) = [I(x,y,z),...        
        norm(cP-[x y z])];
end
% }

disp('Kmeans');
k = 5;
ind = kmeans(X,k);

labelImg = zeros(size(I));

for i=1:k
    labelImg(ind == i) = i;
end

figure; imagesc(makeTileImage(labelImg));
%}


