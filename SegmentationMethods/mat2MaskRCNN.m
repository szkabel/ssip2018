function t = mat2MaskRCNN(inputPath, imagePath, imageNames)
%This functions creates the table that can be used to train the MaskRCNN.
%inputPath is a folder variable, whilst imageNames contain the name of
%those images without extention that are supposed to be used for the training
%imagePath where the separate images are located

N = numel(imageNames);
ROIstrs = {};
paths = {};
h = waitbar(0);
load(fullfile(inputPath,'examDataMap.mat'));
for i=1:N
    waitbar(i/N,h,sprintf('Wait to create training table %d/%d',i,N));    
    examData = examDataMap(imageNames{i});
    if examData.sliceHeight ~= 320 || examData.sliceWidth ~= 320 % we deal only with 320 by 320 images
        continue;
    end
    ROIstr = convertSeriesToROI(examData);    
    for j=1:examData.noSlices
        if ROIstr.Z <= j && j<= ROIstr.Z+ROIstr.Zw
            ROIstrs{end+1} = [ROIstr.Y ROIstr.X ROIstr.Yw ROIstr.Xw];
            paths{end+1} = fullfile(imagePath,[imageNames{i} '-' num2str(j,'%03d') '.png']);
        %elseif rand < 0.3 %add only approx 1/3 of the empty ones
        %    ROIstrs{end+1} = [];
        %    paths{end+1} = fullfile(imagePath,[imageNames{i} '-' num2str(j,'%03d') '.png']);
        end        
    end
end
close(h);

t = table(paths',ROIstrs','VariableNames',{'imageFileName','ligaments'});
end