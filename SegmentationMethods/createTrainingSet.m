function [trainingImageNames,trainingIndices,testImageNames,testIndices,trainingTable] = createTrainingSet

imagePath = 'D:\�bel\Iskola\SZTE\SSIP\Graz2018\data\separateImages';
dataPath = 'D:\�bel\Iskola\SZTE\SSIP\Graz2018\data\rawMat';

d = dir([dataPath filesep '*mat']);
d(end) = []; % get rid of examDataMap

learningRate = 2/3;
randomIndices = randperm(numel(d));

separator = round(length(randomIndices)*learningRate);
trainingIndices = randomIndices(1:separator);
testIndices = randomIndices(separator+1:end);

trainingImageNames = {d(trainingIndices).name};
testImageNames = {d(testIndices).name};

for i=1:numel(trainingImageNames)
    [~,trainingImageNames{i},~] = fileparts(trainingImageNames{i});       
end
for i=1:numel(testImageNames)
    [~,testImageNames{i},~] = fileparts(testImageNames{i});    
end

trainingTable = mat2MaskRCNN(dataPath, imagePath, trainingImageNames);

end

