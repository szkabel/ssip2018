function [prevMax,postMax ] = findLocMins( fun )
%This functions finds which is the closest local minimum before and after
%the local maximum which is the closest to the center. Fun is an array that contains the
%function's function values

[~,maxi] = findpeaks(fun);

centerPos = length(fun)/2;
[~,minIndFromCenter] = min(abs(centerPos - maxi));
maxi = maxi(minIndFromCenter);

i = maxi+1;
while i<length(fun) && fun(i)<fun(i-1)
    i = i+1;
end
postMax = i;

i = maxi-1;
while i>0 && fun(i)<fun(i+1)
    i = i-1;
end
prevMax = i;


end

