function threeDBoundingBox = predictRCNN(rcnn,img)
%Function to create a 3D bounding box in ROIstr format (ROI structure see
%convertFromSeriesToROI.m) 
%If the output is empty it means that we didn't find any images yet

noSlice = size(img,3);
thresh = 0.8;

allScores = zeros(1,noSlice);
allBBs = NaN(noSlice,4);
for i=1:noSlice
    [bbox, score, ~] = detect(rcnn, img(:,:,i), 'MiniBatchSize', 32);
    
    if ~isempty(score)
        [score, idx] = max(score);
        allBBs(i,:) = bbox(idx, :);
        allScores(i) = score;    
    end
end

foundSg = filterScores(allScores,thresh);
if any(foundSg) %if not all of them are empty
    BBs = allBBs(foundSg,:);
    scores = allScores(foundSg);
    threeDBoundingBox.X = mean(BBs(:,2));
    threeDBoundingBox.Y = mean(BBs(:,1));
    threeDBoundingBox.Xw = mean(BBs(:,4));
    threeDBoundingBox.Yw = mean(BBs(:,3));
    threeDBoundingBox.Z = find(foundSg,1,'first');
    threeDBoundingBox.Zw = find(foundSg,1,'last')-threeDBoundingBox.Z;
    threeDBoundingBox.meanScore = mean(scores);
else
    threeDBoundingBox = [];
end

end

